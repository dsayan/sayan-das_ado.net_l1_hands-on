﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        
        public Form2()
        {
            InitializeComponent();
            SqlConnection con = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=CAR;Integrated Security=True");
            string final_text="";

            con.Open();

            SqlCommand cmd = new SqlCommand(@"SELECT [CarType] FROM [dbo].[CAR]", con);
            SqlDataReader sdr = cmd.ExecuteReader();
            while(sdr.Read())
            {
                final_text+= sdr.GetString(0)+Environment.NewLine;
            }
            CarNames.Text = final_text;
            
            con.Close();
        }

    }
}
