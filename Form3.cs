﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            SqlConnection con = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=CAR;Integrated Security=True");
            SqlCommand cmd = new SqlCommand(@"SELECT RegistrationNumber,CarType FROM [dbo].[CAR]",con);
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable table1 = new DataTable();
            da.Fill(table1);
            comboBox1.DataSource = table1;
            comboBox1.DisplayMember = "RegistrationNumber";
            comboBox1.ValueMember = "CarType";
        }

        public string final_text = "";
        private void button1_Click(object sender, EventArgs e)
        {
            label4.Text = final_text;

        }
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            final_text = comboBox1.SelectedValue.ToString();
        }
    }
}
